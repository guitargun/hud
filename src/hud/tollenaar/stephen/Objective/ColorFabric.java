package hud.tollenaar.stephen.Objective;

import org.bukkit.ChatColor;

public class ColorFabric {

	public static ChatColor CalcColor(int base, int max) {
		int percentage = (int) (((base + 0.0)/ (max + 0.0))*100);
		if (percentage < 10) {
			return ChatColor.DARK_RED;
		}
		if (percentage < 20) {
			return ChatColor.RED;
		}
		if (percentage < 30) {
			return ChatColor.LIGHT_PURPLE;
		}
		if (percentage < 40) {
			return ChatColor.DARK_PURPLE;
		}
		if (percentage < 50) {
			return ChatColor.GOLD;
		}
		if (percentage < 60) {
			return ChatColor.YELLOW;
		}
		if (percentage < 70) {
			return ChatColor.AQUA;

		}
		if (percentage < 80) {
			return ChatColor.DARK_AQUA;
		}
		if (percentage < 90) {
			return ChatColor.DARK_GREEN;
		} else {
			return ChatColor.GREEN;
		}
	}
}
