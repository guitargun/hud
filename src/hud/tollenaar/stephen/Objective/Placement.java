package hud.tollenaar.stephen.Objective;

import hud.tollenaar.stephen.CEvents.TargetBlockPlaceEvent;
import hud.tollenaar.stephen.General.HUDmain;
import hud.tollenaar.stephen.Minion.Attacker;
import hud.tollenaar.stephen.Minion.HealthBarMob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import net.minecraft.server.v1_8_R3.EntityMonster;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class Placement implements Listener {

	private HUDmain plugin;
	private static HashMap<Integer, List<Attacker>> attackers = new HashMap<Integer, List<Attacker>>();
	private static HashMap<Integer, HealthBarMob> healthbars = new HashMap<Integer, HealthBarMob>();

	public Placement(HUDmain instance) {
		this.plugin = instance;
	}

	@EventHandler
	public void onTargetPlacement(BlockPlaceEvent event) {
		if (event.getItemInHand().getItemMeta() != null
				&& event.getItemInHand().getItemMeta().getDisplayName() != null) {
			if (event.getItemInHand().getItemMeta().getDisplayName()
					.equalsIgnoreCase("Target")
					&& event.getItemInHand().getItemMeta().getLore() != null) {
				TargetBlockPlaceEvent target = new TargetBlockPlaceEvent(
						event.getBlock(), event.getItemInHand().getItemMeta()
								.getLore(), event.getPlayer());
				Bukkit.getServer().getPluginManager().callEvent(target);
			}
		}
	}

	@EventHandler
	public void onTargetMaking(TargetBlockPlaceEvent event) {
		Block block = event.getBlock();
		final Player player = event.getPlayer();
		String[] raw = event.getInfo().get(0).split("/");
		String hp = ColorFabric.CalcColor(Integer.parseInt(raw[0]),
				Integer.parseInt(raw[1]))
				+ raw[0] + ChatColor.GRAY + "/" + ChatColor.GREEN + raw[1];
		Location loc = block.getLocation();

		int lvl = Integer.parseInt(event.getInfo().get(1));

		final int id = Integer.parseInt(event.getInfo().get(2));

		final List<Attacker> att = new ArrayList<Attacker>();

		for (int i = 3; i < event.getInfo().size(); i++) {
			String minion = event.getInfo().get(i).split(":")[0];
			int amount = Integer.parseInt(event.getInfo().get(i).split(":")[1]);
			att.addAll(Attacker.SpawnMinions(loc, plugin,
					EntityType.valueOf(minion.toUpperCase()), lvl, amount, id, player));
		}

		loc.setY(loc.getY() + 1);
		loc.setX(loc.getX() + 0.5);
		loc.setZ(loc.getZ() + 0.5);

		healthbars.put(id, new HealthBarMob(loc, hp, plugin, id, player));

		attackers.put(id, att);

	}

	@EventHandler
	public void onAIAttack(EntityDamageByEntityEvent event) {
		if (event.getEntity().getType() == EntityType.SILVERFISH
				&& ((CraftEntity) event.getDamager()).getHandle() instanceof EntityMonster) {
			String[] oh = ChatColor.stripColor(
					event.getEntity().getPassenger().getCustomName())
					.split("/");
			int max = Integer.parseInt(oh[1]);
			int base = (int) (Integer.parseInt(oh[0]) - event.getDamage());

			event.setCancelled(true);

			int id = event.getEntity().getPassenger().getMetadata("quest")
					.get(0).asInt();
			if (base > 0) {
				String nh = ColorFabric.CalcColor(base, max)
						+ Integer.toString(base) + ChatColor.GRAY + "/"
						+ ChatColor.GREEN + max;
				event.getEntity().getPassenger().setCustomName(nh);
				event.getEntity()
						.getWorld()
						.playSound(event.getEntity().getLocation(),
								Sound.ANVIL_LAND, 1, 0);

			} else {
				event.getEntity()
						.getWorld()
						.playSound(event.getEntity().getLocation(),
								Sound.ANVIL_BREAK, 1, 0);
				StopProtect(id);
			}
		}else if(event.getEntity() instanceof Player && ((CraftEntity) event.getDamager()).getHandle() instanceof EntityMonster && event.getDamager().hasMetadata("player")){
			Player player = (Player) event.getEntity();
			UUID monstercarry = (UUID) event.getDamager().getMetadata("player").get(0).value();
			if(!monstercarry.equals(player.getUniqueId())){
				event.setCancelled(true);
			}
		}
	}

	private void StopProtect(int id) {
		for (Attacker at : attackers.get(id)) {
			at.destroy();
		}
		healthbars.get(id).destroy();
	}

	@EventHandler
	public void onAIDeath(EntityDeathEvent event) {
		if (event.getEntity().hasMetadata("protect")) {
			int[] array = (int[]) event.getEntity().getMetadata("protect")
					.get(0).value();

			Attacker at = attackers.get(array[1]).get(array[2]);
			at.destroy();
			List<Attacker> begin = attackers.get(array[1]);
			begin.remove(array[2]);

			List<Attacker> list = new ArrayList<Attacker>();
			for (Attacker in : begin) {
				in.setid(list.size());
				in.setnewmetadata(list.size());
				list.add(in);
			}
			if (list.size() == 1) {
				attackers.remove(array[1]);
				healthbars.get(array[1]).destroy();
				healthbars.remove(array[1]);
			} else {
				attackers.put(array[1], list);
			}
		}
	}

	@EventHandler
	public void OnPassengerPickUp(PlayerPickupItemEvent event) {
		if (event.getItem().hasMetadata("quest")) {
			event.setCancelled(true);
		}
	}
}
