package hud.tollenaar.stephen.Minion;

import hud.tollenaar.stephen.General.EntityHider;
import hud.tollenaar.stephen.General.HUDmain;
import hud.tollenaar.stephen.General.EntityHider.Policy;
import net.minecraft.server.v1_8_R3.EntitySilverfish;
import net.minecraft.server.v1_8_R3.GenericAttributes;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;



public class HealthBarMob extends EntitySilverfish {

	private HUDmain plugin;
	private EntityHider hider;

	public HealthBarMob(Location loc, String hp, HUDmain pl, int id,
			Player player) {
		super(((CraftWorld) loc.getWorld()).getHandle());
		this.plugin = pl;
		this.hider = new EntityHider(pl, Policy.BLACKLIST);
		this.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(),
				loc.getPitch());
		((CraftWorld) loc.getWorld()).getHandle().addEntity(this);
		AICleaner();
		AddItem(hp, id, player);
		
	
		for (Player in : Bukkit.getOnlinePlayers()) {
			if(in.getUniqueId() != player.getUniqueId()){
			hider.toggleEntity(in, this.getBukkitEntity());
			}
		}

	}

	public void destroy() {
		this.getBukkitEntity().getPassenger().remove();
		this.getBukkitEntity().remove();
	}

	private void AICleaner() {
		this.getAttributeInstance(GenericAttributes.maxHealth).setValue(1000D);
		this.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(0D);
		this.getAttributeInstance(GenericAttributes.FOLLOW_RANGE).setValue(0D);
		this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED)
				.setValue(0D);
		this.getAttributeInstance(GenericAttributes.c).setValue(10000D);
	}

	private void setName(org.bukkit.entity.Entity item, String hp) {
		item.setCustomName(hp);
		item.setCustomNameVisible(true);
	}

	private void AddItem(String hp, int id, Player player) {
		Item item = this
				.getBukkitEntity()
				.getLocation()
				.getWorld()
				.dropItem(this.getBukkitEntity().getLocation(),
						new ItemStack(Material.EMERALD_BLOCK));
		item.setVelocity(new Vector(0, 0, 0));
		item.setFallDistance(0);
		setName(item, hp);
		item.setMetadata("quest", new FixedMetadataValue(plugin, id));

		this.getBukkitEntity().setPassenger(item);
		
		for (Player in : Bukkit.getOnlinePlayers()) {
			if (!in.getUniqueId().equals(player.getUniqueId())) {
				hider.toggleEntity(in, item);
			}
		}

	}

}
