package hud.tollenaar.stephen.Minion;

import org.bukkit.Location;

import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.PathfinderGoal;

public class PathfinderGoalWalkTo extends PathfinderGoal {

		private final float speed;
		private final EntityInsentient insentient;
		private final Location to;

		public PathfinderGoalWalkTo(EntityInsentient entityInsentient, Location to, float speed) {
			insentient = entityInsentient;
			this.speed = speed;
			this.to = to;
		}
		
		
		@Override
		public void c() {
			insentient.getNavigation().a(to.getX(), to.getY(), to.getZ(), speed);
		}


		@Override
		public boolean a() {
//			if(insentient.getBukkitEntity().getLocation().distance(to) <= 2){
//				System.out.println("had to return");
//				return false;
//			}
			return true;
		}
}
