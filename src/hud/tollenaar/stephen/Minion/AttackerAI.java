package hud.tollenaar.stephen.Minion;

import hud.tollenaar.stephen.General.HUDmain;

import java.util.UUID;

import net.minecraft.server.v1_8_R3.EntityCreature;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.adamki11s.pathing.AStar;
import com.adamki11s.pathing.AStar.InvalidPathException;
import com.adamki11s.pathing.Tile;

public class AttackerAI extends BukkitRunnable {

	private EntityCreature minion;
	private Location target;
	private HUDmain plugin;

	public AttackerAI(HUDmain instance, EntityCreature minion, Location target) {
		this.minion = minion;
		this.target = target;
		this.plugin = instance;

		runTaskTimer(plugin, 10L, 10L);
	}

	private EntityCreature getMinion() {
		return minion;
	}

	private Location getTarget() {
		return target;
	}

	public void run() {
		try {
			Location start = getMinion().getBukkitEntity().getLocation();
			while (start.getBlock().getType() == Material.AIR) {
				start.setY(start.getY() - 1);
			}

			Location end = getTarget();
			while (end.getBlock().getType() == Material.AIR) {
				end.setY(end.getY() - 1);
			}

			AStar path = new AStar(start, end, (int) getMinion()
					.getBukkitEntity().getLocation()
					.distanceSquared(getTarget()));
			boolean walk = true;

			// attacking the player

			for (Entity near : getMinion().getBukkitEntity().getNearbyEntities(
					5, 5, 5)) {
				if (near instanceof Player) {
					UUID monstercarry = (UUID) getMinion().getBukkitEntity()
							.getMetadata("player").get(0).value();
					Player pl = (Player) near;
					if (monstercarry.equals(pl.getUniqueId())) {
						WalktoLocation(near.getLocation());
						walk = false;
					}
				}
			}

			if (walk) {
				int i = 0;
				try {
					// making the walking towards the target AI
					for (Tile tiles : path.iterate()) {
						if (i == 0) {
							i++;
						} else {
							Location l = tiles.getLocation(getMinion()
									.getBukkitEntity().getLocation());
							while (l.getBlock().getType() == Material.AIR) {
								l.setY(l.getY() - 1);
							}
							WalktoLocation(l);
							break;
						}
					}
				} catch (NullPointerException ex) {
					// make the attacking AI
				}
			}
		} catch (InvalidPathException e) {
			// System.out.println(e.getErrorReason());
		}
	}

	public void WalktoLocation(Location l) {
		getMinion().goalSelector.a(0, new PathfinderGoalWalkTo(getMinion(), l,
				1.5F));
	}

}
