package hud.tollenaar.stephen.Minion;

import hud.tollenaar.stephen.General.EntityHider;
import hud.tollenaar.stephen.General.HUDmain;
import hud.tollenaar.stephen.General.EntityHider.Policy;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.server.v1_8_R3.EntityCreature;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.PathfinderGoalHurtByTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_8_R3.PathfinderGoalNearestAttackableTarget;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.BlockIterator;

import com.adamki11s.pathing.AStar;
import com.adamki11s.pathing.PathingResult;
import com.adamki11s.pathing.AStar.InvalidPathException;

public class Attacker {
	private HUDmain plugin;

	private EntityCreature minion;
	private AttackerAI AI;
	private int attackerid;

	private EntityHider hider;

	private void PacketFabric(Player player, EntityCreature crea) {
		for (Player in : Bukkit.getOnlinePlayers()) {
			if (!in.getUniqueId().equals(player.getUniqueId())){
			hider.toggleEntity(in, crea.getBukkitEntity());
			}
		}
	}

	private Attacker(Location target, HUDmain plugin, Location spawn,
			EntityType type, int lvl, int id, int attacker, Player player) {
		this.plugin = plugin;
		this.minion = spawn(spawn, type, lvl, id, player);
		this.AI = new AttackerAI(plugin, minion, target);
		this.hider = new EntityHider(plugin, Policy.WHITELIST);
		PacketFabric(player, minion);
	}

	public static List<Attacker> SpawnMinions(Location target, HUDmain plugin,
			EntityType type, int lvl, int amount, int id, Player player) {
		List<Attacker> attackers = new ArrayList<Attacker>();
		for (Location loc : buildCircle(target, 25, amount)) {
			attackers.add(new Attacker(target, plugin, loc, type, lvl, id,
					attackers.size(), player));

		}
		return attackers;
	}

	private EntityCreature spawn(Location spawnloc, EntityType type, int lvl,
			int id, Player pl) {
		Entity ent = spawnloc.getWorld().spawnEntity(spawnloc, type);
		int[] info = new int[] { lvl, id, attackerid };
		ent.setMetadata("protect", new FixedMetadataValue(plugin, info));
		ent.setMetadata("player", new FixedMetadataValue(plugin, pl.getUniqueId()));
		EntityCreature cr = (EntityCreature) ((CraftEntity) ent).getHandle();
		DefaultGoals(cr);
		return cr;
	}

	public void destroy() {
		AI.cancel();
		minion.getBukkitEntity().remove();
	}

	private static List<Location> buildCircle(Location center, int radius,
			int amount) {
		World world = center.getWorld();

		while (center.getBlock().getType() == Material.AIR) {
			center.setY(center.getY() - 1);
		}

		double increment = (2 * Math.PI) / amount;
		ArrayList<Location> locations = new ArrayList<Location>();
		for (int i = 0; i < amount; i++) {
			double angle = i * increment;
			double x = center.getX() + (radius * Math.cos(angle));
			double z = center.getZ() + (radius * Math.sin(angle));
			Location adding = new Location(world, x, center.getY(), z);
			while (adding.getBlock().getType() == Material.AIR) {
				adding.setY(adding.getY() - 1);
			}
			try {
				AStar path = new AStar(adding, center,
						(int) adding.distance(center));
				path.iterate();

				if (path.getPathingResult() == PathingResult.SUCCESS) {
					adding.setY(adding.getY() + 1);
					locations.add(adding);
				} else {

					BlockIterator it = new BlockIterator(world,
							center.toVector(), center.setDirection(
									adding.toVector()).toVector(), 0,
							(int) center.distance(adding));
					while (it.hasNext()) {
						Location next = it.next().getLocation();
						while (next.getBlock().getType() == Material.AIR) {
							next.setY(next.getY() - 1);
						}


						AStar p2 = new AStar(center, next,
								(int) center.distance(next));
						p2.iterate();
						if (path.getPathingResult() == PathingResult.SUCCESS) {
							next.setY(next.getY() + 1);
							locations.add(next);
							break;
						}
					}
				}
			} catch (InvalidPathException e) {
				System.out.println(e.getErrorReason());
			}

		}
		return locations;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void DefaultGoals(EntityCreature minion) {

		minion.goalSelector.a(2, new PathfinderGoalMeleeAttack(minion,
				HealthBarMob.class, 1D, true));
		minion.goalSelector.a(1, new PathfinderGoalMeleeAttack(minion,
				EntityHuman.class, 1D, true));

		minion.targetSelector
				.a(1, new PathfinderGoalHurtByTarget(minion, true));
		minion.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(
				minion, EntityHuman.class, true));
		minion.targetSelector.a(3, new PathfinderGoalNearestAttackableTarget(
				minion, HealthBarMob.class, true));
	}

	public void setid(int id) {
		attackerid = id;
	}

	public void setnewmetadata(int attackerid) {
		int[] array = (int[]) minion.getBukkitEntity().getMetadata("protect")
				.get(0).value();
		array[2] = this.attackerid;
		minion.getBukkitEntity().setMetadata("protect",
				new FixedMetadataValue(plugin, array));
	}
}
