package hud.tollenaar.stephen.General;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import lib.fanciful.main.FancyMessage;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class Commands implements CommandExecutor {

	private HUDmain plugin;
	private PlayerHUD hud;
	
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("hud") && sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length != 0) {
				if (args[0] != null) {
					if (args[0].equalsIgnoreCase("target")) {
						ItemStack target = new ItemStack(Material.SPONGE);
						ArrayList<String> info = new ArrayList<String>();
						ItemMeta temp = target.getItemMeta();
						temp.setDisplayName("Target");
						info.add("100/100");
						info.add("5");
						info.add("0");
						info.add("Zombie:5");
						info.add("Spider:2");
						temp.setLore(info);
						target.setItemMeta(temp);
						player.setItemInHand(target);
					}
				}
			}

			return true;
		} 

		return true;
	}

	public Commands(HUDmain instance) {
		this.plugin = instance;
		this.hud = new PlayerHUD(instance);
	}


	
}
