package hud.tollenaar.stephen.General;

import hud.tollenaar.stephen.Objective.Placement;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PlayerConnection;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.metadata.PlayerMetadataStore;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;


public class HUDmain extends JavaPlugin implements Listener {
	private Method removePlayerInfo;
	
	

	public void onEnable() {

		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new Placement(this), this);
		pm.registerEvents(this, this);
		pm.registerEvents(new SpawnListener(this), this);
		getCommand("hud").setExecutor(new Commands(this));
	}

	public void onDisable() {
		
	}

	
	
}
