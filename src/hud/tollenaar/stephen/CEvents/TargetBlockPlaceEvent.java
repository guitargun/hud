package hud.tollenaar.stephen.CEvents;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TargetBlockPlaceEvent extends Event{

	private Block block;
	private Player player;
	
	public Block getBlock() {
		return block;
	}

	public List<String> getInfo() {
		return info;
	}

	private List<String> info;
	
	
	public TargetBlockPlaceEvent(Block block, List<String> info, Player player){
		this.block = block;
		this.info = info;
		this.player = player;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public Player getPlayer() {
		return player;
	}



	private static final HandlerList handlers = new HandlerList();
}
